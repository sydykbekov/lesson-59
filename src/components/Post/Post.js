import React, {Component} from 'react';
import './Post.css';

class Post extends Component {
    componentWillMount() {
        console.log('[Post] WillMount');
    }

    componentDidMount() {
        console.log('[Post] DidMount');
    }

    componentWillUpdate() {
        console.log('[Post] WillUpdate');
    }

    componentDidUpdate() {
        console.log('[Post] DidUpdate');
    }

    shouldComponentUpdate(newProps, newState) {
        console.log('[Post] ShouldUpdate');
        return (newProps.title !== this.props.title || newProps.author !== this.props.author);
    }

    render() {
        console.log('[Post] render');
        return (
            <article className="Post">
                <h1>{this.props.title}</h1>
                <div className="Info">
                    <div className="Author">{this.props.author}</div>
                </div>
            </article>
        )
    }
}

export default Post;