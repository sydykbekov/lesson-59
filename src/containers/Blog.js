import React, {Component, Fragment} from 'react';
import './Blog.css';
import Post from "../components/Post/Post";

class Blog extends Component {
    state = {
        posts: [
            {title: 'Test Posts', author: 'John Doe', id: '1'},
            {title: 'Hello, world!', author: 'Jack Black', id: '2'},
            {title: 'Another Example', author: 'Main Editor', id: '3'}
        ],
        postsFormShown: false
    };


    togglePostsForm = () => {
        this.setState(prevState => {
            return {propsFormShown: !prevState.postsFormShown}
        });
    };

    componentDidMount() {
        const POSTS_URL = 'https://jsonplaceholder.typicode.com/posts?_limit=4';
        fetch(POSTS_URL).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong with network request');
        }).then(posts => {
            const updatedposts = posts.map(post => {
                return {...post, author: 'John Doe'};
            });
            this.setState({posts: updatedposts});
        }).catch(error => {
            console.log(error);
        });

        /*const response = await fetch(POSTS_URL);
        if (response.ok) {
            console.log(response);
            let updatedPosts = await response.json();

            updatedPosts = updatedPosts.map(post => {
                return {...post, author: 'John Doe'};
            });

            this.setState({posts: updatedPosts});
        } else {
            console.log('Network error');
        }*/
    };

    render() {
        let postsForm = null;

        if (this.state.postsFormShown) {
            postsForm = (
                <section className="NewPost">
                    <p>New post form will be here</p>
                </section>
            );
        }

        return (
            <Fragment>
                <section className="Posts">
                    {this.state.posts.map(post => (
                        <Post key={post.id} title={post.title} author={post.author} />
                    ))}
                </section>
                <button className="ToggleButton" onClick={this.togglePostsForm}>New Post</button>
                {postsForm}
            </Fragment>
        );
    }
}

export default Blog;