import React, {Component} from 'react';
import './App.css';
import Blog from "./containers/Blog";

class App extends Component {
    render() {
        return <Blog />;
    }
}

export default App;
